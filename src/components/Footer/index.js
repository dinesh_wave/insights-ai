import React from 'react';
import PropTypes from 'prop-types';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import {
  IconButton,
  BottomNavigation,Typography,InputBase,BottomNavigationAction} from '@material-ui/core';
import { fade } from '@material-ui/core/styles/colorManipulator';
import { withStyles } from '@material-ui/core/styles';
import PersonIcon from '@material-ui/icons/Person';
import NotificationsIcon from '@material-ui/icons/Notifications';
import SettingsIcon from '@material-ui/icons/Settings';
import ExploreIcon from '@material-ui/icons/Explore';
import DashboardIcon from '@material-ui/icons/Dashboard';

const styles = theme => ({
  root: {
    width: '100%',
    position:'fixed',
    bottom:0,
    //background:'#3480ef',
  },
  grow: {
    flexGrow: 1,
  },
  menuButton: {
    marginLeft: -12,
    marginRight: 20,
  },
  title: {
    display: 'none',
    [theme.breakpoints.up('sm')]: {
      display: 'block',
    },
  },
  search: {
    position: 'relative',
    borderRadius: theme.shape.borderRadius,
    backgroundColor: fade(theme.palette.common.white, 0.15),
    '&:hover': {
      backgroundColor: fade(theme.palette.common.white, 0.25),
    },
    marginLeft: 0,
    width: '100%',
    [theme.breakpoints.up('sm')]: {
      marginLeft: theme.spacing.unit,
      width: 'auto',
    },
  },
  searchIcon: {
    width: theme.spacing.unit * 9,
    height: '100%',
    position: 'absolute',
    pointerEvents: 'none',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  inputRoot: {
    color: 'inherit',
    width: '100%',
  },
  inputInput: {
    paddingTop: theme.spacing.unit,
    paddingRight: theme.spacing.unit,
    paddingBottom: theme.spacing.unit,
    paddingLeft: theme.spacing.unit * 10,
    transition: theme.transitions.create('width'),
    width: '100%',
    [theme.breakpoints.up('sm')]: {
      width: 120,
      '&:focus': {
        width: 200,
      },
    },
  },
  icons:{
    //color: '#ffffff'
  },
  navigationLinks:{
      maxWidth:'100px'
  }
});

function Footer(props) {
  const { classes } = props;
  return (
    <BottomNavigation
      showLabels
      className={classes.root}
    >
      <BottomNavigationAction  icon={<DashboardIcon className={classes.icons} />} className={classes.navigationLinks} />
      <BottomNavigationAction  icon={<NotificationsIcon className={classes.icons} />} className={classes.navigationLinks} />
      <BottomNavigationAction  icon={<ExploreIcon className={classes.icons} />} className={classes.navigationLinks} />
      <BottomNavigationAction  icon={<PersonIcon className={classes.icons} />} className={classes.navigationLinks} />
      <BottomNavigationAction  icon={<SettingsIcon className={classes.icons} />} className={classes.navigationLinks} />
    </BottomNavigation>
  );
}

Footer.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Footer);
