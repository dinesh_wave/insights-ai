/** @format */

import React, {useEffect} from 'react'
import Batch from './containers/Batch'
import Home from './containers/Home'
import {Route, Switch, BrowserRouter as Router} from 'react-router-dom'
import Scanning from './containers/Processing/Scanning'
import Streaming from './containers/Streaming'
import axios from 'axios'

export default function App() {
	/** Generate auth token and store in local storage to use it to access APIs */
	useEffect(() => {
		axios({
			method: 'get',
			headers: {
				'X-Vault-Token': 's.Nbia9A4aru7i1JgfGDuVIcU5'
			},
			url: 'http://35.239.71.246:8200/v1/kv/data/token'
		})
			.then((res) => {
				localStorage.setItem('auth_token', res.data?.data?.data?.token || '')
			})
			.catch((err) => {
				console.log(err)
			})
	}, [])
	return (
		<Router>
			<Switch>
				<Route path='/' exact component={Home} />
				<Route path='/batch' exact component={Batch} />
				<Route path='/scanning' exact component={Scanning} />
				<Route path="/streaming" exact component={Streaming}/>
			</Switch>
		</Router>
	)
}
