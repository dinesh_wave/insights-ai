import React, { useEffect } from "react";
import Layout from "../../components/Layout";
import { Grid } from "@material-ui/core";
import { withStyles } from "@material-ui/core/styles";
import 'react-circular-progressbar/dist/styles.css';
import { createMuiTheme, MuiThemeProvider } from "@material-ui/core/styles";

const styles = (theme) => ({
    container: {
        [theme.breakpoints.only("lg")]: {
            height: `calc(100vh - 165px)`,
        },
        [theme.breakpoints.only("md")]: {
            height: `calc(100vh - 165px)`,
        },
        [theme.breakpoints.only("xl")]: {
            height: `calc(100vh - 165px)`,
        },
        width: "100%",
    },
    heading:{
      fontSize: '32px',
      marginTop:"5%"
    },
    circularProgress: {
      width: '23%',
      marginTop: '5%'
    },
    buttonStyle: {
      fontWeight: "normal",
      padding: "10px 40px 10px 40px",
      border:'2px solid #000000',
      color:"#000000"
  },

});

const muiCardTheme = createMuiTheme({
  overrides: {
    MuiStepIcon: {
      text: {
        fill: "white",
      },
      
      root:{
        '&$completed': {
          color: '#ffffff',
        },
        '&$active': {
          color: '#3480ef',
        },
       
      }
    }
  }
});

function getSteps() {
  return ['scanning', 'enhancing'];
}

function Scanning(props) {
    const { classes } = props;
    const [bgImage, setBgImage] = React.useState({});
    const steps = getSteps();

    useEffect(()=>{
      setBgImage({
        backgroundImage:`url(${props.location.state.file})`,
        backgroundRepeat:'no-repeat',
        backgroundAttachment:'fixed',
        backgroundSize:'100% 100%',
        backgroundAttachment:'inherit'
      })
    },[]);

    return ( 
        <MuiThemeProvider theme={muiCardTheme}>
        <Layout> 
        <Grid container spacing = {2} justify="center" className={classes.container}>
        <Grid item xs={4} style={bgImage} align="center"> 
        </Grid>
        <Grid item xs={4} align="center">
          <p>{props.location.state.resText}</p>
        </Grid>
        </Grid> 
        </Layout>
        </MuiThemeProvider>
    );
}
export default withStyles(styles)(Scanning);