import React, {useState} from 'react'
import Layout from '../../components/Layout'
import {Button, Grid, Snackbar} from '@material-ui/core'
import {withStyles} from '@material-ui/core/styles'
import axios from 'axios'
import {DataGrid} from '@material-ui/data-grid'
import MuiAlert from '@material-ui/lab/Alert'
import {CSVDownload} from 'react-csv'
import { Link } from 'react-router-dom'

function Alert(props) {
	return <MuiAlert elevation={6} variant='filled' {...props} />
}

const csv = require('csvtojson')
const styles = (theme) => ({
	button: {
		margin: theme.spacing.unit
	},
	input: {
		display: 'none'
	},
	container: {
		[theme.breakpoints.only('lg')]: {
			height: `calc(100vh - 165px)`
		},
		[theme.breakpoints.only('md')]: {
			height: `calc(100vh - 165px)`
		},
		[theme.breakpoints.only('xl')]: {
			height: `calc(100vh - 165px)`
		},
		width: '100%'
	},
	buttonStyle: {
		fontWeight: 'normal',
		padding: '7px 35px 7px 35px'
	},
	fileUploadContainer: {
		border: '1px solid #e6dbdb',
		display: 'flex',
		justifyContent: 'space-between',
		width: '36vw',
		margin: 'auto'
	},
	fullSizeImg: {
		width: '50%',
		height: 'auto'
	},
	gridItem: {
		marginTop: '2%',
		marginBottom: '2%'
	},
	alertSuccess: {
		color: '#3c763d',
		backgroundColor: '#dff0d8',
		borderColor: '#d6e9c6',
		padding: '20px'
	},
	alertWarning: {
		color: '#8a6d3b',
		backgroundColor: '#faebcc',
		borderColor: '#faebcc',
		padding: '20px'
	},
	alertDanger: {
		color: '#a94442',
		backgroundColor: '#f2dede',
		borderColor: '#ebccd1',
		padding: '20px',
		width: 'fit-content',
		margin: 'auto'
	},
})

function Covid(props) {
	const {classes} = props
	const [filePreview, setFilePreview] = useState({
		event: ''
	})
	const [showLoader, setShowLoader] = useState(false)
	const [responseText, setResponseText] = useState({
		csv: [],
		status: ''
	})
	const [open, setOpen] = useState(false)
	const [cols, setCols] = useState([])
	const [rows, setRows] = useState([])
	const handleChange = (event) => {
		if (event.target && event.target.files.length) {
			setResponseText({
				csv: [],
				status: ''
			})
			setFilePreview({
				...filePreview,
				event: event
			})
		}
	}

	const handleClose = (event, reason) => {
		if (reason === 'clickaway') {
			return
		}
		setOpen(false)
	}

	const scanImage = () => {
		const authToken = 'Bearer ' + localStorage.getItem('auth_token')
		setResponseText({
			csv: [],
			status: ''
		})
		var bodyFormData = new FormData()
		bodyFormData.append('file', filePreview.event.target.files[0])
		setShowLoader(true)
		axios({
			method: 'post',
			url: 'https://us-central1-insights-318308.cloudfunctions.net/rul-model-process-noauth',
			data: bodyFormData,
			headers: {Authorization: authToken}
		})
			.then(async (res) => {
				let rows = []
				let columns = []
				let num = 1
				if (res.data) {
					await csv({
						colParser: {
							column1: 'omit',
							column2: 'string'
						},
						checkType: true
					})
						.fromString(res.data)
						.subscribe((jsonObj) => {
							delete jsonObj.field1
							rows.push({...jsonObj, id: num})
							if (num === 1) {
								for (var key in rows[0]) {
									let colObj = {
										field: key,
										headerName: key.toUpperCase()
									}
									if (key === 'id') {
										colObj.hide = true
									}
									columns.push(colObj)
								}
							}
							num++
						})
					setRows(rows)
					setCols(columns)
					setShowLoader(false)
					setResponseText({
						csv: res,
						status: '200'
					})
				}
			})
			.catch((err) => {
				setShowLoader(false)
				setOpen(true)
				setResponseText({
					csv: [],
					status: '500'
				})
			})
	}
	return (
		<Layout>
			<h2 align='center' style={{fontFamily: 'ui-monospace'}}>
				Predictive RUL of sensor data in CSV data file
			</h2>
			<Grid container spacing={2} align='center' className={classes.gridItem}>
				<Grid item xs={12}>
					<div className={classes.fileUploadContainer} style={{padding: '0%'}}>
						<p
							style={{
								paddingLeft: '15px',
								color: 'rgba(0, 0, 0, 0.38)'
							}}
						>
							{filePreview.event && filePreview.event.target.files[0] && filePreview.event.target.files[0].name}
						</p>
						<label htmlFor='outlined-button-file'>
							<Button variant='outlined' component='span' className={classes.button}>
								Upload
							</Button>
						</label>
						<input accept='image/*' className={classes.input} id='outlined-button-file' type='file' onChange={handleChange} />
					</div>
				</Grid>
				<Grid item xs={12}>
					{showLoader ? (
						'Loading...'
					) : (
						<Button
							variant='outlined'
							color='primary'
							className={classes.buttonStyle}
							onClick={scanImage}
							disabled={!filePreview.event || !filePreview.event.target.files[0] || !filePreview.event.target.files[0].name}
						>
							Submit
						</Button>
					)}
				</Grid>
			</Grid>
			{responseText.status == '200' && (
				<div
					style={{
						height: 400,
						width: '90vw',
						margin: 'auto'
					}}
				>
					<DataGrid rows={rows} columns={cols} pageSize={10} />
					<CSVDownload data={rows} target='_blank' />
				</div>
			)}
			{responseText.status == '500' && (
				<Snackbar open={open} autoHideDuration={6000} onClose={handleClose}>
					<Alert severity='error'>Something went wrong!!</Alert>
				</Snackbar>
			)}
		</Layout>
	)
}
export default withStyles(styles)(Covid)
