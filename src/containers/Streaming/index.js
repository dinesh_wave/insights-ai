import { Button, CircularProgress, Tooltip } from '@material-ui/core';
import React, { useState } from 'react';
import { withStyles } from '@material-ui/core/styles';
import { composeClasses } from '@material-ui/data-grid';
import Layout from '../../components/Layout';
import axios from 'axios';

const styles = (theme) => ({
    container: {
        textAlign: '-webkit-center'
    },
    mainDiv: {
        width: '50vw',
        // marginTop:'1rem',
        // border:'1px solid black',
        display: 'flex',
        justifyContent: 'space-evenly'
    },
    charts: {
        marginTop: '2rem',
        padding: '1rem',
        width: '75vw',
    },
    imgWidth: {
        width: '-webkit-fill-available',
        height: "65vh"
    }
})

function Stream(props) {
    const [start, setStart] = useState('');
    const { classes } = props;
    const handleStart = () => {
        axios({
            method: 'get',
            url: 'http://127.0.0.1:5005/start',
        })
            .then(async () => {
                await setStart('start');
                console.log('server started sucessfully')
            })
            .catch((err) => {
                console.log(err);
                setStart('start');
            })
    }

    const handleStop = () => {
        axios({
            method: 'get',
            url: 'http://127.0.0.1:5005/stop',
        }) 
            .then(async ()=> {
                await setStart('stop');
                console.log('server stopped sucessfully')
            })
            .catch((err)=>{
                console.log(err)
                setStart('stop');
            })
    }

    return (
        <Layout>
            <div className={classes.container}>
                <h2>STREAMING - Sensor Prediction</h2>
                <div className={classes.mainDiv}>
                    <Tooltip title="Deploying the tools and libraries required for the pipeline"><Button variant="contained" color="primary" size="large">Deploy</Button></Tooltip>
                    <Tooltip title="Starting the stream of sending messages from database to model running inside Kafka.">
                        <Button
                            onClick={handleStart}
                            variant="contained"
                            color="primary"
                            size="large"
                            disabled={start==='start'}
                            >
                            start
                        </Button>
                    </Tooltip>
                    <Tooltip title="Terminating the stream">
                        <Button
                            onClick={handleStop} 
                            variant="contained" 
                            color="primary" 
                            disabled={start==='stop'}
                            size="large">
                            stop
                        </Button>
                    </Tooltip>
                </div>
                <div className={classes.charts}>
                    {start === " " ?

                    // <a href='http://localhost:8086/orgs/27972f66b3196245/dashboards/09be32f1dfc14000?lower=now%28%29%20-%201h' target='__blank'><img className={classes.imgWidth} src='./charts.png' /></a>
                    // <iframe style={{width:'-webkit-fill-available', height:'55vh', textAlign:'center'}} srcDoc="<h1>Please start the server to see the data</h1><CircularProgress/>" title='demo'></iframe>
                    <>
                    <h3>Please start the server to see the data</h3>
                    <CircularProgress/>
                    </>
                    :
                    <>
                    {start==='start' ? <h3 style={{color:'green'}}>Server Started Successfully...</h3> : start==='stop' ? <h3 style={{color:'red'}}>Server Stopped Successfully...</h3> : <h3></h3> } 

                    <div style={{position:'relative',width:'80vw',height:'58vh',overflow:'hidden'}}>
                    <iframe style={{position:'absolute',width:'80vw',height:'86vh',left:'-82px',top:'-100px'}} title='dhashboard charts' src='http://localhost:8086/orgs/27972f66b3196245/dashboards/09d9ce5a09f3b000?lower=now%28%29%20-%206h'></iframe></div>

                    </>                    }
                </div>
            </div>
        </Layout>
    )
}

export default withStyles(styles)(Stream);