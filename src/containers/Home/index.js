import React from 'react'
import Layout from '../../components/Layout'
import { Button, Grid, Snackbar, Tooltip } from '@material-ui/core'
import { withStyles } from '@material-ui/core/styles'
import { Link } from 'react-router-dom'

const styles = (theme) => ({
	title: {
		textAlign: 'center'
	},
	window1: {
		marginTop: '2rem',
		textAlign: 'center',
		//border:'1px solid black',
		width: '100vw',
		display: 'flex',
		justifyContent: 'center'
	},
	grid: {
		borderRadius: '10px',
		width: '20rem',
		minHeight: '10rem',
		maxHeight: '20rem',
		backgroundColor: "#3307bf57",
		margin: '0 20px'
	}
})
const Home = (props) => {
	const { classes } = props;
	return (
		<Layout>
			<h1 className={classes.title}>Sensor Prediction</h1>
			<p className={classes.title}>The Sensor Prediction use case deals with the prediction of a machine life cycle which is RUL. </p>
			<Grid className={classes.window1}>
				<Grid item className={classes.grid}>
					<a style={{ textDecoration: "none", color: '#150c55' }} target='__blank' href='http://35.223.9.21:3004/'>
						<Tooltip title="Batch scenario is specific to sending a batch size of data and getting the results for that specific batch.">
							<h1 style={{ marginTop: '3.5rem' }}>BATCH</h1>
						</Tooltip>
					</a>
					<p>Batch scenario is specific to sending a batch size of data and getting the results for that specific batch.</p>
				</Grid>
				<Grid item className={classes.grid}><Link style={{ textDecoration: "none", color: '#150c55' }} to="/streaming"><Tooltip title="Streaming scenario defines the process of sending continuous messages from source(or any) to server and get results."><h1 style={{ marginTop: '3.5rem' }}>STREAMING</h1></Tooltip></Link> <p>Streaming scenario defines the process of sending continuous messages from source(or any) to server and get results. </p></Grid>
			</Grid>
		</Layout>
	)
}

export default withStyles(styles)(Home);