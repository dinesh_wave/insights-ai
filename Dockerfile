FROM node:10.16.3-alpine
MAINTAINER Wavelabs
RUN mkdir /app
WORKDIR /app
ADD ./package.json /app/package.json
ADD . /app
RUN npm set progress=false
RUN npm install -g serve --loglevel=error && npm install --loglevel=error
RUN npm install -g react-scripts --loglevel=error
CMD ["npm","start"]